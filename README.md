# AttachmentCount for Thunderbird

AttachmentCount adds an optional column to the thread pane showing the number of attachments in a message. Use the thread pane columnpicker to select the Attachment Counts column and then drag it to whatever position you like.
